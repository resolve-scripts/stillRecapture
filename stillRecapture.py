# Written by Alex Golding 2020


import sys
import os
import sqlite3
import time
try:
	import psycopg2
	remote = True
except ModuleNotFoundError:
	print(f"Warning psycopg package not found, remote databases will not work\n" \
			"Please check with your system administrator and refer to Readme for installation tips")
	remote = False

# TODO Make this compatible with external scripting
try:
    # This is to throw an exception if running externally to resolve
    resolve
except NameError:
    # Importing as bmd to mirror the way python is setup in Resolve
    import DaVinciResolveScript as bmd
    resolve = bmd.scriptapp("Resolve")
    
__version__ = '2022291026.01'



# resolve = app.GetResolve()
fu = resolve.Fusion()
pm = resolve.GetProjectManager()
project = pm.GetCurrentProject()
tmls = int(project.GetTimelineCount())
projectName = project.GetName()
dbInfo = pm.GetCurrentDatabase()
ui = fu.UIManager
disp = bmd.UIDispatcher(ui)
gallery = project.GetGallery()

# Setup a warning window
myWarningWindow = disp.AddWindow({ "WindowTitle": "Warning",
									"ID": "warnWin",
									"Geometry": [ 600, 300, 280, 110 ],
									"WindowModality": "WindowModal", },
	[
		ui.VGroup({ "Spacing": 10, "Weight": 0.0,},
			[

				ui.Label({ "ID": "warnLabel", "Text": "Warning",}),
				ui.HGroup({ "Spacing": 50, "Weight": 0.0, },
					[
						ui.Button({ "ID": "CancelBtn", "Text": "Cancel", }),
						ui.Button({ "ID": "contBtn", "Text": "Continue", })
					]),

			]),
	])

itm = myWarningWindow.GetItems()
warningLbl = itm['warnLabel']

def _func(ev):
	print("Exited")
	disp.ExitLoop()
	myWarningWindow.Hide()
	exit()
myWarningWindow.On.warnWin.Close = _func

def _func(ev):
	print("Cancelled")
	disp.ExitLoop()
	myWarningWindow.Hide()
	exit()
myWarningWindow.On.CancelBtn.Clicked = _func


def _func(ev):
	disp.ExitLoop()
	myWarningWindow.Hide()
myWarningWindow.On.contBtn.Clicked = _func


# We need to make sure the Albums actually have a name in the database
galleryDict = {}
galleries = gallery.GetGalleryStillAlbums()
for g in galleries:
	name = gallery.GetAlbumName(g)
	galleryDict[name] = g
	gallery.SetAlbumName(g, name)

# We need the database to be up to date
consoleShowing = fu.IsConsoleShowing()
if consoleShowing:
	fu.ShowConsole(False)
warningLbl.Text = "Save project"
myWarningWindow.Show()
disp.RunLoop()
myWarningWindow.Hide()
fu.ShowConsole(consoleShowing)	# return console to previous state
if not pm.SaveProject():
	print("Save project failed")
	exit()

# Lets go get a database
if dbInfo['DbType'] == 'Disk':
	print("Disk database")
	dbName = dbInfo['DbName']
	projDbBasePath = ''
	dbList = ''
	# First lets find the disk database
	# We need to get to dblist.conf to locate the database
	# TODO add support for windows and linux
	if sys.platform.startswith("darwin"):
		dbList = os.path.join(os.path.expanduser('~'), 'Library', 'Preferences', 'Blackmagic Design', 'DaVinci Resolve', 'dblist.conf')
		print(dbList)
	else:
		print("Platform not yet supported")
		exit()
	if os.path.exists(dbList):
		with open(dbList, 'r') as d:
			for line in d:
				# We need to add the ":" to the dbName as it is possible to have a Postgres db of the same name in dblist.conf
				if line.startswith(dbName + ":"):
					projDbBasePath = line.split(':')[1]
					projDbBasePath = os.path.join(projDbBasePath, 'Resolve Projects', 'Users', 'guest', 'Projects')
					break
	else:
		print("Unable to find dblist.conf")
		exit()
	if os.path.isdir(projDbBasePath):
		print(f'Sucess, Projects found at {projDbBasePath}')
	else:
		print(projDbBasePath)
	folders = []

	if pm.GetCurrentFolder() != '':	# Means we're in a folder
		#print("Project folder is {}".format(pm.GetCurrentFolder()))
		folders.append(pm.GetCurrentFolder())
		while pm.GotoParentFolder():
			folders.append(pm.GetCurrentFolder())
		folders.pop() # Should always be '' i.e. root and can be removed from the list
		folders.reverse()
		#print(folders)
		for folder in folders:
			if not pm.OpenFolder(folder):
				print(f"Failed to open {folder}")
		dbPath = os.path.join(projDbBasePath, *folders)
	else:
		dbPath = projDbBasePath
	dbPath = os.path.join(dbPath, projectName, "Project.db")
	if not os.path.exists(dbPath):
		print("Unable to find project databases")
		exit()
	# SQlite uses '?' for including query parameters
	qp = '?'
	# Lets open the DB as read only
	db = sqlite3.connect("file:{}?mode=ro".format(dbPath), uri=True)
	# we need a cursor to the database
	cursorObj = db.cursor()
elif dbInfo['DbType'] == 'PostgreSQL':
	if remote:
		print("PostgreSQL database")
		# SQlite uses '%s' for including query parameters
		qp = '%s'
		try:
			db = psycopg2.connect(
				host=dbInfo['IpAddress'],
				database=dbInfo['DbName'],
				user="postgres",
				password="DaVinci")
		except psycopg2.OperationalError:
			print("Wrong password")
		cursorObj = db.cursor()
	else:
		print("Unable to open DB")
		exit()
else:
	print("Unknown DB type")
	exit()

drvVer = resolve.GetVersion()
print(drvVer)

# We need a dictionary with names as keys and index as value
timelines = {} 
for x in range(tmls):
	idx = x+1
	timeline = project.GetTimelineByIndex(idx)
	timelines[timeline.GetName()] = idx

# Now to setup our GUI window
myWindow = disp.AddWindow({ "WindowTitle": "Stills recapture", "ID": "stillGrabWin", "Geometry": [ 600, 300, 280, 110 ], },
	[
		ui.VGroup({ "Spacing": 10, "Weight": 0.0,},
			[
				ui.HGroup({ "Spacing": 0, "Weight": 0.0, },
					[
						ui.Label({ "ID": "fromGalLabel", "Text": "Re Grab stills from",}),
						ui.ComboBox({ "ID": "fromGalCombo",}),
					]),
				ui.HGroup({ "Spacing": 0, "Weight": 0.0, },
					[
						ui.Label({ "ID": "toGalLabel", "Text": "Save new stills to",}),
						ui.ComboBox({ "ID": "toGalCombo",}),
					]),
				ui.HGroup({ "Spacing": 50, "Weight": 0.0, },
					[
						ui.Button({ "ID": "CancelBtn", "Text": "Cancel", }),
						ui.Button({ "ID": "GrabBtn", "Text": "Grab", })
					]),
			]),
	])

itm = myWindow.GetItems()

# We need a list of gallery names, and can't use the API method as this also returns PowerGrade albums
cursorObj.execute(f'SELECT "ViewName" \
					FROM "Gallery::GyView"\
					WHERE "Gallery::GyGallery_id"\
					IN (SELECT  "Gallery::GyGallery_id"\
					FROM "Gallery::GyGallery"\
					WHERE "SM_Project_id"\
					IN (SELECT "SM_Project_id"\
					FROM "SM_Project"\
					WHERE "ProjectName" = {qp}));', (projectName,))
galls = cursorObj.fetchall()

print(galls)
# Dict/list (see python to lua scripting) for our values
metaFields = {1:"All except output"}
cnt = 2
for g in galls:
	metaFields[cnt] = g[0]
	cnt += 1

itm['fromGalCombo'].AddItems(metaFields)
itm['fromGalCombo'].CurrentIndex = 0

current = gallery.GetAlbumName(gallery.GetCurrentStillAlbum())
print(f"Current Stills album: {current}")

cnt = 1
metaFields = {}
idx = 0
for g in galls:
	metaFields[cnt] = g[0]
	if g[0] == current:
		idx = cnt - 1 # the index is zero referenced, the lua list/dict is not!
		print(f"{g[0]} - This is the one we should select {cnt}")
	cnt += 1

itm['toGalCombo'].AddItems(metaFields)
itm['toGalCombo'].CurrentIndex = idx
print(f"{itm['toGalCombo'].CurrentText} - This is the one we have select {itm['toGalCombo'].CurrentIndex} ")

# Vital function, all GUI windows need at least one method to close them!
def _func(ev):
	print("Exited")
	disp.ExitLoop()
	myWindow.Hide()
	exit()
myWindow.On.stillGrabWin.Close = _func

def _func(ev):
	print("Cancelled")
	disp.ExitLoop()
	myWindow.Hide()
	exit()
myWindow.On.CancelBtn.Clicked = _func

# This doesn't do much, but allows the script to move forward
def _func(ev):
	disp.ExitLoop()
myWindow.On.GrabBtn.Clicked = _func

# Let's display the window
myWindow.Show()
disp.RunLoop()
myWindow.Hide()

# How were things set when we finished with the window
fromGallery = itm['fromGalCombo'].GetCurrentText()
toGallery = itm['toGalCombo'].GetCurrentText()
print(f"Capturing stills from: {fromGallery}")
print(f"Capturing new stills to: {toGallery}")



# First check the output gallery is empty, display warning if it's not
if galleryDict[toGallery].GetStills():
	print(f"{toGallery} is not empty")
	warningLbl.Text = f"{toGallery} is not empty"
	consoleShowing = fu.IsConsoleShowing()
	if consoleShowing:
		fu.ShowConsole(False)
	myWarningWindow.Show()
	disp.RunLoop()
	myWarningWindow.Hide()
	fu.ShowConsole(consoleShowing)	# return console to previous state
	
# Switch to out output gallery
gallery.SetCurrentStillAlbum(galleryDict[toGallery])


# Query the database for timeline and timecode info for the stills
# TODO also get label for the stills (found in "Gallery::GyStillRef" -> "label")
if fromGallery == "All except output":
	print("Getting all stills")
	# Get all the stills refs from all other galleries
	cursorObj.execute(f'SELECT "SrcHint", "RecTC" \
						FROM "Gallery::GyStill" \
						WHERE "Gallery::GyStill_id" \
						IN (SELECT "Ref0"\
						FROM "Gallery::GyStillRef"\
						WHERE "Gallery::GyView_id"\
						IN (SELECT "Gallery::GyView_id"\
						FROM "Gallery::GyView"\
						WHERE "Gallery::GyGallery_id"\
						IN (SELECT  "Gallery::GyGallery_id"\
						FROM "Gallery::GyGallery"\
						WHERE "SM_Project_id"\
						IN (SELECT "SM_Project_id"\
						FROM "SM_Project"\
						WHERE "ProjectName" = {qp})) \
						AND NOT "ViewName" = {qp})) \
						ORDER BY "SrcHint", "RecTC";', (projectName, toGallery))
else:
	# Get all stills refs from specified gallery
	print(f"Getting all stills from {fromGallery}")
	cursorObj.execute(f'SELECT "SrcHint", "RecTC" \
						FROM "Gallery::GyStill" \
						WHERE "Gallery::GyStill_id" \
						IN (SELECT "Ref0"\
						FROM "Gallery::GyStillRef"\
						WHERE "Gallery::GyView_id"\
						IN (SELECT "Gallery::GyView_id"\
						FROM "Gallery::GyView"\
						WHERE "Gallery::GyGallery_id"\
						IN (SELECT  "Gallery::GyGallery_id"\
						FROM "Gallery::GyGallery"\
						WHERE "SM_Project_id"\
						IN (SELECT "SM_Project_id"\
						FROM "SM_Project"\
						WHERE "ProjectName" = {qp})) \
						AND "ViewName" = {qp})) \
						ORDER BY "SrcHint", "RecTC" ;', (projectName, fromGallery))

still_refs = cursorObj.fetchall()

# Done with reading databases
db.close()

# Finally let's grab the sills
prevTline, prevTc = None, None
for tline, tc in still_refs:
	timeout = 20
	if tline == prevTline and tc == prevTc:
		print("Warning duplicate detected")
	else:
		try:
			tlnum = timelines[tline]
		except KeyError:
			print("Timeline not in project, skipping")
			continue
		timeline = project.GetTimelineByIndex(tlnum)
		if project.SetCurrentTimeline(timeline): # returns true if it succeeds
			#print(f"Setting TC to {tc}")
			if timeline.SetCurrentTimecode(tc):
				# We need to make sure the correct TC is applied before grabbing a still
				while timeline.GetCurrentTimecode() != tc:
					print(f"Waiting for the correct frame {timeout}")
					time.sleep(0.1)
					timeout -= 1
					if not timeout:
						print("Setting TC timed out")
						break
				print(f"Grabbing still from {tline} at {timeline.GetCurrentTimecode()}")
				# Time to grab a still
				timeline.GrabStill()
			else:
				print(f"Failed to set timecode to {tc}")
		else:
			print(f"Failed to set timeline to {tline}")
	prevTline, prevTc = tline, tc

print("Finished")
