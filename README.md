# stillRecapture README

Copyright (c) 2022 Alex Golding

## A handy script to search for and re grab stills

When a still is made in DaVinci Resolve it represents a snapshot of the timeline at that frame, any changes to the colour of a clip after creation are not represented in the still. The only option to update a still is to grab a new one at the same point in the timeline. This script automates the process allowing for whole galleries or even a whole projects worth of stills to be recaptured reflecting any changes made to the grade since they were originally grabbed.

stillRecapture is a script designed mainly to be run from within DaVinci Resolve and works with either regular or studio.

Currently only works on Osx, still with limited testing, your milage may vary. If you require this script to work with Windows or Linux please raise an issue over on GitLab. Support should be very simple to add, but be prepared to help.

Testing using the script externally with Resolve Studio and/or postgres is very limited. This is due to not having a "disposable" system with these set up, and tinkering with a system you can't afford to break is never a good idea. If you wish to use this script with either it should work fine, but I would recommend approaching with the same caution.

## Usage

It is recommended to capture stills into an empty gallery. Unfortunately it is not possible to create a gallery via the API, therefore you should make one before running the script.

To run from within Resolve go to 'Workspace->Scripts->stillRecapture'

The project must be saved in order to ensure the database is up to date, you will be asked to confirm this. Click 'Continue' to save.

Check which gallery you wish to recapture stills from and to. Default is all galleries except the output gallery, and outputting to the currently selected one.

If the output gallery is not empty you will be prompted to cancel or continue.

If nothing happens or no dialogs appear check the console to see if there's any information about the problem.

#### Caveats

* This relies on the timecode of the timeline when the still was first captured. Changes to the timeline will cause a different frame to get captured, potentially from a different clip.
* Duplicate frames will be ignored.
* Stills can only be recaptured from ones created within the current project and from the same timeline. Imported stills or ones from deleted timelines will be ignored.
* At the moment it does not support copying existing labels, however any stills created will be labelled as per any automatic labeling rules as set in the projects settings.
* PowerGrade albums are ignored by this script.
* If you have changed folders in project manager since opening the project this script will likely fail, simply re-open the project to ensure your back in the right folder and try again.

#### Dependancies

* Python3 
	* If you plan to run the script externally check Help->Documentation->Developer and see the Scripting README.txt on how to setup environment variables
	* To use within Resolve python must be setup in the 'Fusion' page, for V17 you must have python 3.6. For V18 and presumably onwards you can use any python version from 3.6+, see below.
* For a disk database that's it
* For a remote database `psycopg2` python library need to be installed. This is not a simple pip command see [installation instructions](https://www.psycopg.org/docs/install.html). Contact your sysadmin to install, I found the it simple enough, but if something goes wrong you could potentially break Resolves connection to the remote database. Backup before attempting!
	* If on Mac OS and you have homebrew already set up the simplest method that works for me is ```brew install postgresql``` followed by ```pip3 install psycopg2```

#### Preparation

If you intend to run the script from within either Resolve or Resolve studio then Python must be setup correctly.

If using Resolve 17.4 then this needs to specifically be Python 3.6, unfortunately this is now end of life, however it should still be available for download here [Python3.6.8](https://www.psycopg.org/docs/install.html). As this is an old version you should be cautious about if it will cause anything else relying on a newer version of python on your system to break (unlikely but possible)

If using Resolve 18 or above then any version of Python from 3.6 onwards can be used. Unless you have any specific requirements or already have Python installed then it's recommended to use the latest (3.10 at time of writing)

With Python installed you now need to confirm that Resolve can see it properly. Open any project in Resolve and switch to the 'Fusion' page. From the top menus go to 'Fusion->Fusion Settings...' Then go to the 'Script' section of the dialog that opens and make sure Python 3 is selected. Save the 'Fusion Settings' and open the console window (Workspace->console), at the top of the console window click on 'Py3', if Resolve doesn't complain about not finding Python then congratulations all you need to do is put the script where Resolve can see it.


stillRecapture.py should be copied into:

On Mac OS:

For all users
	
		'/Library/Application Support/Blackmagic Design/DaVinci Resolve/Fusion/Scripts/Comp/'

For only one user

		'/Users/<your username>/Library/Application Support/Blackmagic Design/DaVinci Resolve/Fusion/Scripts/Comp/'
		
		
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


